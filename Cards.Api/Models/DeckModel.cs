﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cards.Api.Models
{
    public class DeckModel
    {
        public class Deck
        {
            public int id { get; set; }
            public List<Card> listOfCards { get; set; }
        }
    }
}
