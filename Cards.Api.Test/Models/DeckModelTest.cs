﻿using Cards.Api.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using static Cards.Api.Models.DeckModel;

namespace Cards.Api.Test.Models
{
    class DeckModelTest
    {
        private Deck deck;
        private readonly int IdDeck = 4;
        private readonly int idCard = 2;

        [SetUp]
        public void Setup()
        {
            this.deck = new Deck()
            {
                id = IdDeck,
                listOfCards = new List<Card>() {
                  new Card(){
                    Id=idCard,
                    Suit=Suit.Heart,
                    Value= Value.King
                  }
                }

            };
        }


        [Test]
        public void GetDeckId()
        {
            Assert.AreEqual(this.deck.id, this.IdDeck);
        }

        [Test]
        public void GetCardCount()
        {
            Assert.AreEqual(this.deck.listOfCards.Count, 3);
        }


    }
}