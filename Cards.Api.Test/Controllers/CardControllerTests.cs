// <copyright file="CardControllerTest.cs" company="Staywell">
// Copyright (c) Staywell. All rights reserved.
// </copyright>

namespace Cards.Api.Controllers.Test
{
    using System;
    using System.Collections.Generic;
    using Cards.Api.Controllers;
    using Cards.Api.Data;
    using Cards.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using NUnit.Framework;
    using static Cards.Api.Models.DeckModel;

    public class CardControllerTests
    {
        private CardController cardsController;
        private CardsContext cardsContext;

        [SetUp]
        public void Setup()
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            this.cardsContext = new CardsContext(new DbContextOptionsBuilder<CardsContext>()
                .UseInMemoryDatabase(databaseName: $"Cards_{rnd.Next()}")
                .Options);

            DataGenerator.Initialize(this.cardsContext);

            this.cardsController = new CardController(cardsContext);
        }

        [Test]
        public void Get_Invoked_ReturnsResults()
        {
            // Arrange

            // Act
            List<Card> cards = this.cardsController.Get();

            // Assert
            Assert.NotNull(cards);
            Assert.NotZero(cards.Count);
        }

        [Test]
        public void Get_InvokedTwice_ReturnsDifferentResults()
        {
            // Arrange

            // Act
            List<Card> hand1 = this.cardsController.Get();
            List<Card> hand2 = this.cardsController.Get();

            // Assert
            Assert.NotNull(hand1);
            Assert.NotNull(hand2);
            Assert.AreNotEqual(hand1, hand2);
        }



        [Test]
        public void GetDecks_Invoke_ReturnNumberOfDecks()
        {

            List<Deck> deck_testing_numberOfDecks4 = this.cardsController.GetDecks(4);
            List<Deck> deck_testing_numberOfDecks0 = this.cardsController.GetDecks(0);
            List<Deck> deck_testing_numberOfDecks1 = this.cardsController.GetDecks(1);


            Assert.NotNull(deck_testing_numberOfDecks4);
            Assert.NotNull(deck_testing_numberOfDecks0);
            Assert.NotNull(deck_testing_numberOfDecks1);

            Assert.AreEqual(4, deck_testing_numberOfDecks4);
            Assert.AreEqual(1, deck_testing_numberOfDecks0);
            Assert.AreEqual(1, deck_testing_numberOfDecks1);

            Assert.AreNotEqual(deck_testing_numberOfDecks4[1], deck_testing_numberOfDecks4[0]);
            Assert.AreNotEqual(deck_testing_numberOfDecks4[1], deck_testing_numberOfDecks4[2]);
            Assert.AreNotEqual(deck_testing_numberOfDecks4[1], deck_testing_numberOfDecks4[3]);

        }
    }
}